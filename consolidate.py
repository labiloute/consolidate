#!/usr/bin/env python
#-*- coding: utf-8 -*-

############
# Internal
###########
# To use exit, sys.version, sys.argv, etc
import sys
# Os,to iterate thru directories
import os
# To read and write yaml files
#import yaml (pip3 install oyaml)
import yaml as yaml
import io
# To read csv file
import csv
# To get cli arguments
import getopt

############
# DB modules
############
# pip3 install mysql-connector-python
from include import mysql
# pip3 install pymssql
from include import mssql
# pip3 install cx_Oracle
from include import oracle
# To read csv files
from include import csv

################
# Logging module
################
from include import CLogs
Logs=CLogs.CLogs()


"""
# READ YAML CONFIG FILE
"""

# Read YAML Main config file
with open("config/config.yaml", 'r') as stream:
	data_loaded = yaml.safe_load(stream)
	#data_loaded = yaml.safe_dump(stream,line_break=1)
	#data_loaded = yaml.load(stream)
config=data_loaded

"""
# Read all queries files
"""

directory_path = config['queries']['path']
files = [f for f in os.listdir(directory_path) if os.path.isfile(os.path.join(directory_path, f)) and f.endswith(".yaml")]
Logs.Log("INFO","Found {nbqueries} query files".format(nbqueries=len(files)),hidedate=True)

Logs.Log("DEBUG","Connecting to local db {db} on host {host}".format(db=config['db']['database'],host=config['db']['host']))
mdest=mysql.MysqlDB(None,config,Logs)

helppage="""
Welcome to consolidate :)
Usage :
-q --query		: execute only a specific query (type it with ending .yml)
-f --force		: force execution of a disabled query
-i --intervalmonth	: force variable intervalmonth for all run queries
-n --nolog		: do not log result in log files
-h --help		: print this help message
--show-query		: print query used after all modifications
"""

###################################################
log=True
selquery=False
force=False
showquery=False
intervalmonth=False

# Try to grab arguments from user. "send" and "mail" are the same, they launch mail sending
try:
    # Get option. Short Argument are -f or -q . Q need to be followed by value, that is why it is followed by a :
    # Same way, long arguments are follow by a = if they require a value.
    opts, args = getopt.getopt(sys.argv[1:],"ihfnq:",["show-query","force","nolog","help","query=","intervalmonth="])
    # Parse all Args
    for arg in args:
        #if arg in ('send','mail'):
        #    dryrun=False
        pass
    # Parse all Options
    for name, value in opts:
        # Force intervalmonth
        if name in ("--intervalmonth", "-i"):
            intervalmonth=value
        # No log (unimplemented yet)
        if name in ("--nolog","-n"):
            log=False
        # Evaluate query
        if name=="-q" or name=="--query":
            selquery=value
        # Evaluate forcing
        if name in ("-f", "--force"):
            force=True
        # Display help page
        if name in ("-h", "--help"):
            print(helppage)
            sys.exit(0)
        if name in "--show-query":
            showquery=True
# Raise an error on wrong parameters given
except getopt.GetoptError as e:
    print(helppage)
    print(e)
    sys.exit(1)

# Display messages
#if dryrun:
    Logs.Log("WARNING","Running dryrun. Not implemented yet")
if selquery:
    Logs.Log("WARNING","Running a single query file named : \"{}\"".format(selquery))
if not log:
    Logs.Log("WARNING","Logging disabled. Not implemented yet")


###################################################

for queryfile in files:

	##########################
	# INFO AND PREPARATION
	##########################

	# Break if query requested by not in first arg
	if selquery and selquery!=str(queryfile):
		continue

	# Read YAML file
	with open("{directory_path}/{queryfile}".format(directory_path=directory_path,queryfile=queryfile), 'r') as stream:
		source = yaml.safe_load(stream)

	# Info
	Logs.Log("INFO","############################################",hidedate=True)
	Logs.Log("INFO","Working on file {queryfile}".format(queryfile=queryfile),hidedate=True)
	Logs.Log("INFO","############################################",hidedate=True)

	# Get Table name from yaml file name
	#table=queryfile.split(".",1)[0]

	# Set table name from source config or from file name
	if 'table' in source and source['table']:
		table=source['table']
	else:
		table=queryfile.split(".",1)[0]
	Logs.Log("DEBUG","Table name used will be {table}".format(table=table))

	# Display comment if set
	if 'description' in source:
		Logs.Log("INFO","{}".format(source['description']),hidedate=True)

	# Create objects, depending on driver
	Logs.Log("DEBUG","Driver is {driver} ".format(driver=source['driver']))

	# Break if query not enabled
	if source['enabled']!=True and not force:
		Logs.Log("DEBUG","Query is {color}disabled{none} in config".format(color=Logs.color['ERROR'],none=Logs.color['NONE']))
		continue

	# Notice if query forced
	if source['enabled']!=True and force:
		Logs.Log("WARNING","Forcing execution of a disabled query file")

	######################
	# Or execute full process
	######################

	# [OK] Initial logging
	if 'db' in source and 'host' in source['db']:
	    Logs.Log("DEBUG","Connecting to {host} and getting query model. Please wait.".format(host=source['db']['host']))

	# [OK] Create Instance
	if source['driver']=='mysql':msource=mysql.MysqlDB(config,source,Logs)
	elif source['driver']=='mssql':msource=mssql.MssqlDB(config,source,Logs)
	elif source['driver']=='oracle':msource=oracle.OracleDB(config,source,Logs)
	elif source['driver']=='csv':msource=csv.csvDB(config,source,Logs)

	# Force intervalmonth if provided by cli
	msource.intervalmonth=intervalmonth

	# Break if connexion fails
	if not msource.conn:
		Logs.Log("DEBUG","Connection failed. Jumping to next query")
		continue

	# [OK] Modify  query to add date
	q = msource.addStartDate()

	# [OK] Get model from table
	if 'skipmodel' not in source or not source['skipmodel']:
		model=msource.getQueryModel(q,table)
		# Handle an error getting query model
		if not model:
			Logs.Log("ERROR","Error getting query model. Continuing with next query.")
			continue
	else:
		Logs.Log("WARNING","Skipping query model as requested with \"skipmodel\" option")

	# Get dict paie value:type
	if 'fields' not in source: source['fields']=None
	fields=msource.getfields(source['fields'])

	# Eventually drop table if requested
	if 'dropbeforeinsert' in source and source['dropbeforeinsert'] and ('skipmodel' not in source or not source['skipmodel']):
		Logs.Log("WARNING","Dropping table {} first as requested by config file (dropbeforeinsert)".format(table))
		mdest.query("DROP TABLE `{table}`;".format(table=table),commit=True)

	# [OK] Insert model into table
	if 'skipmodel' not in source or not source['skipmodel']:
		mdest.query(model)

	# [OK ]Query Data from original table
	Logs.Log("DEBUG","Getting data from server... Please wait.")
	if showquery: print(q)
	data=msource.query(q)

	# Set primary Key on destination table
	if 'primary' in source:
		Logs.Log("DEBUG","Setting primary key {primarykey}".format(primarykey=source['primary']))
		mdest.query("ALTER TABLE `{table}` ADD PRIMARY KEY(`{primary}`);".format(table=table,primary=source['primary']),commit=True,multi=True)
	else:
		Logs.Log("WARNING","No primary key defined. Please add 'primary' with field name as value to your query.")

	# Insert data into DB
	Logs.Log("DEBUG","Inserting/Updating {rowcount} row(s) found, into {table}. Please be patient...".format(table=table,rowcount=msource.rowcount))
	mdest.insert(data,table,fields)

	# Closing connections
	Logs.Log("DEBUG","Finished with {rowcount} row(s) affected. Closing source".format(rowcount=mdest.insertrowcount))
	msource.close()

	# Deleting if required - source - allowed only if a primary key is set, and if dropbeforeinsert is disabled
	if ('delete_after' in source and source['delete_after']==True 
		and ('dropbeforeinsert' not in source or source['dropbeforeinsert']==False)
		and 'primary' in source):
		mdest.DelDiff(table,source['primary'],source['delete_filter'],source['delete_mode'],msource.getIntervalMonth(),msource.getStartDate())

	# Eventually log query has run into specific table
	if ('logs' in config and 'lastupdate' in config['logs'] and config['logs']['lastupdate']):
		Logs.Log("DEBUG","Updating table '{lastupdate}' with current timestamp for table with id '{table}'".format(lastupdate=config['logs']['lastupdate'],table=table))
		mdest.LogRun(table)


##############################
# Closing destination database
##############################
if mdest:
	Logs.Log("INFO","Closing connexion with local db {db} on host {host}".format(db=config['db']['database'],host=config['db']['host']))
	mdest.close()
