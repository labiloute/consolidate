# Installation
pip3 install -r requirements.txt

# To load oracle connector library, put this in your .bashrc file :

export ORACLE_HOME="/path/to/consolidate/lib/oracle_instantclient_12_2"
export LD_LIBRARY_PATH="${ORACLE_HOME}:${LD_LIBRARY_PATH}"

# To run consolidate in a crontab, here is a sample :

# Go into installation dir, source bashrc (for Oracle driver env vars), run consolidate at 8.10am and 12.10am, and log into basic log file
10 8,12 * * *    cd /usr/local/bin/consolidate/ && . /root/.bashrc && python3 consolidate.py > /var/log/consolidate.log 2>&1

# Modifying a query

## Modifying table structure :

ALTER TABLE `Table` ADD `Field` INT(4) NULL AFTER `Another_field` ;
ALTER TABLE `Table` ADD `Text_fliend` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `Another_field` ;

## Creating a static table :
CREATE TABLE IF NOT EXISTS Your_new_table(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, field VARCHAR(100) NOT NULL);
And then :
INSERT INTO Your_new_table (field) VALUES ("Data");
And so son...
