#!/usr/bin/env python
#-*- coding: utf-8 -*-

# Mysql connector
# Source : http://www.mysqltutorial.org/python-mysql-query/
from mysql.connector import MySQLConnection, Error

class MysqlDB():
	def __init__(self,GLOBALCONFIG,CONFIG,Logs):
		# Get App Global Settings
		self.GLOBALCONFIG=GLOBALCONFIG
		# Get CX Settings
		self.CONFIG=CONFIG
		# Init Logs
		self.Logs = Logs
		# Connect at init
		try:
			self.conn = MySQLConnection(**self.CONFIG['db'])
			if self.CONFIG['db']['charset']:
				self.Logs.Log("DEBUG","Setting up charset {} for mysql connexion".format(self.CONFIG['db']['charset']))
				self.query("SET NAMES "+self.CONFIG['db']['charset'])
		except Exception as e:
			self.Logs.Log("ERROR","Unable to connect to DB with options {}".format(self.CONFIG['db']))
			self.Logs.Log("DEBUG","Error was {}".format(e))
			self.conn=False
#
	def getQueryModel(self,q,table):
		# Temporarily give a limit to query will speed up model grabbing - but the limit range must be a representative sample of expected data (TODO : something) !
		if "LIMIT" not in q:
			q+=" LIMIT 100 "
		temp_table="CREATE TEMPORARY TABLE {table} \n {q};\nSHOW CREATE TABLE {table}".format(q=q,table=table)
		#temp_table="CREATE TEMPORARY TABLE xx_temp_table_xx \n {q};\nSHOW CREATE TABLE xx_temp_table_xx".format(q=q)
		model=self.query(q=temp_table,multi=True)
		try:
			sql=model[0][1]
		except Exception as e:
			self.Logs.Log("ERROR", "Running query failed :")
			print(model)
			self.Logs.Log("DEBUG", "{}".format(q))
			return False
		#print(sql.replace("CREATE TEMPORARY TABLE", "CREATE TABLE"))
		if sql:
			sql=sql.replace("CREATE TEMPORARY TABLE", "CREATE TABLE IF NOT EXISTS ")
		#except Exception as e:
		#	self.Logs.Log("[ERROR]", "Unable to get query model, il could be due to a wrong SQL syntax.")
		#	return False
		return sql
#
	def columns(self,table):
		schema=self.query("SELECT * FROM information_schema.columns WHERE table_name='" + table + "';")
		columns=[]
		#print(schema)
		for row in schema:
			#print(row)
			columns.append(row[3])
		#print("Les colonnes : {}".format(columns))
		return columns
#
	"""
	Return an ordered dict of column name and column type
	"""
	def getfields(self,fields):
		return fields
#
	def query(self,q, commit=False,multi=False):
		#print(q)
		# Exit if not query provided
		if not q: return False
		# 
		table=()
		cursor = self.conn.cursor()
		try:
			# Hugly hack to execute multi mysql requests
			# See : https://stackoverflow.com/questions/15288594/update-database-with-multiple-sql-statments
			if multi:
				# Multi queries must be included in a try/except or it will raise an Exception even if queries are commited
				# See : https://stackoverflow.com/questions/54987200/mysql-connector-cursor-execute-proceeds-silently-but-makes-no-changes-despite
				try:
					for result in cursor.execute(q, multi=True):
						pass
				except Exception as e:
					# Get Mysql Error id if provided
					if hasattr(e,'errno'):
						errornumber=e.errno
					else:
						errornumber=False
					# Ignoring some specific errors (see link above)
					if str(e)=='generator raised StopIteration' or errornumber==1068:
						self.Logs.Log("DEBUG", "Ignoring mysql error ({}) specific to multi queries : {}".format(errornumber,e))
					else:
						self.Logs.Log("ERROR", "Error running MYSQL multi query ({}) : {}".format(errornumber,e))
					# Pass anyway
					pass
			else:
				# Run query
				cursor.execute(q)
				#allrows=cursor.fetchall()
				if commit:
					try:
						self.conn.commit()
					except:
						self.Logs.Log("ERROR", "Unable to commit changes, rolling back.")
						self.conn.rollback()
		except Error as err:
			if err.errno == 1068:
				self.Logs.Log("WARNING","Mysql said : {e}".format(e=err.msg))
				#self.Logs.Log("DEBUG","Query trace {q}".format(q=q))
			elif err.errno == 1050:
				self.Logs.Log("WARNING","Mysql said : {e}".format(e=err.msg))
				#self.Logs.Log("DEBUG","Query trace {q}".format(q=q))
			else:
				self.Logs.Log("ERROR","Mysql said : Error {n} - {e}".format(n=err.errno,e=err.msg))
				self.Logs.Log("DEBUG","Query trace {q}".format(q=q))
			#print(err)
			#print("Error Code:", err.errno)
			#print("SQLSTATE", err.sqlstate)
			#print("Message", err.msg)
		"""
		except Error as e:
			print("[WARNING] Warning during querying : {q}".format(q=q))
			print(e)
		"""
		# Retrieve rows, x by x
		for row in self.iter_row(cursor, 10):
			table+=(row,)
		# Get affected rows and last affected row ID
		self.rowcount=cursor.rowcount
		#print(cursor.lastrowid)
		#if self.rowcount>0: self.lastrowid=cursor.lastrowid
		#else: self.lastrowid=False
		#print("LastRowID:"+str(self.lastrowid))
		return table
#
	def insert(self,d,table,fields=False):
		# Init
		counta=0
		self.insertrowcount=0
		self.rowcount=0
		# Get columns names, except if forced by option "fields"
		#print(self.columns(table))
		if not fields:
			cols=self.columns(table)
		else:
			cols=list(fields.keys())

		#print("Les colonnes : {}".format(cols))
		# Keep memory of inserted ID, to notice if duplicates found
		self.inserted_id=[]
		# Fieldlist is used to update only a selected range of fields - if not given, all fields needs to be updated and row count in database MUST match with count of row in data provided
		fieldslist=""
		#print("Number of columns in table : "+len(cols[0]))

		# Prepare fields if provided
		"""
		if fields:
			fieldslist="("
			f=0
			for field in fields:
				if f>0:fieldslist+=","
				fieldslist+="`"+field+"`"
				f+=1
			fieldslist+=")"
		"""
		# Explicitely give fields name in the INSERT query
		fieldslist="("
		f=0
		for field in cols:
			if f>0:fieldslist+=","
			fieldslist+="`"+field+"`"
			f+=1
		fieldslist+=")"
		#
		#print(fieldslist)
		#print(cols)
		#sys.exit(0)
		# For each row "i" of the table "d"
		for i in d:
			dup=""
			#q="REPLACE INTO {table} VALUES(".format(table=table)
			q="INSERT INTO {table}{fieldslist} VALUES(".format(table=table,fieldslist=fieldslist)
			#print(i)
			countb=0
			# For each column "f" of the row "i"
			for f in i:
				# Store the first data value, beacause it is required to be the primary key
				if countb==0:self.inserted_id.append(f)
				#print("Number of columns in data : "+len(f))

				# Try to decode bytearray field (imported from mysql), or convert it to string
				try:
					#print(f)
					if f:
						f = f.decode()
					#print(f)
				except:
					f=str(f)
				# Convert None and zero to NULL
				if f==None or f=='':f='NULL'
				#if not f:f='NULL'
				# Escaping string
				else: f=self.conn.converter.escape(f)
				# Add comma to field except if it's the first one
				#print (f)
				# Add a comma to both INSERT and UPDATE list
				if countb>0:
					q+=","
					dup+=","
				# Add ' to protect field, except for NULL values
				#print(countb)
				if f == 'NULL':
					q+=str(f)
					#if (f!="id" and f!='ID'):
					#dup+="`{}`={}\n".format(str(cols[countb][3]),str(f))
					dup+="`{}`={}\n".format(str(cols[countb]),str(f))
				else:
					q+="'{}'".format(str(f))
					#if (f!="id" and f!='ID'):
					#dup+="`{}`='{}'\n".format(str(cols[countb][3]),str(f))
					try:
						dup+="`{}`='{}'\n".format(str(cols[countb]),str(f))
					except Exception as e:
						self.Logs.Log("ERROR","Unable to prepare fields. This may append if your query returns more columns than your table can collect. Error is : {}".format(e))
				# +1
				countb+=1
			# Close query
			q+=")"
			q+=" ON DUPLICATE KEY UPDATE\n"+dup
			#print(q)
			#sys.exit(0)
			counta+=1
			self.query(q,commit=True)
			self.insertrowcount+=self.rowcount
			# END OF LOOPs parsing each line and col

		# Eventually notice if duplicates inserted
		#print(inserted_id)
		duplicates=self.has_duplicates(self.inserted_id)
		if duplicates and 'ignore_duplicates' not in self.CONFIG:
			self.Logs.Log("WARNING","We found {} duplicates on the first column of your query. If the first column is your primary key, this can mean to a malformed query.".format(duplicates))
			duplist=self.list_duplicates(self.inserted_id)
			self.Logs.Log("WARNING","List of duplicates : {duplicates} ".format(duplicates=duplist))

		elif duplicates and 'ignore_duplicates' not in self.CONFIG:
			self.Logs.Log("DEBUG","Ignoring {} duplicates".format(duplicates))

#
	"""
	Method used to get ids in database based on a filter (usually a date). It can then be compared to inserted ones. Difference can be deleted
	"""
	def DelDiff(self,table,primary,filter,filtermode=False,intervalmonth=False,startdate=False):
		# Init
		diff=[]
		r=False
		datefilter=False

		# Exit if smething missing
		if not filtermode:
			self.Logs.Log("ERROR","Setting is missing for deletion : filtermode. Please read documentation if it exists !")
			return False

		# Construct query
		if filtermode=="intervalmonth":
			self.Logs.Log("DEBUG","Deletion query will be filtered to dataset fresher than {intervalmonth} month".format(intervalmonth=intervalmonth))
			datefilter="DATE(NOW() - INTERVAL {} MONTH)".format(intervalmonth)
		elif filtermode=="startdate":
			self.Logs.Log("DEBUG","Deletion query will be filtered by results starting from {startdate}".format(startdate=startdate))
			datefilter="DATE({})".format(startdate)

		# Stops if there is still no date filter
		if not datefilter:
			self.Logs.Log("ERROR","Setting is missing for deletion : intervalmonth (may be global) or startdate (may be global)")
			return False

		# Simple query to select all ids in database, based on the main filter
		q="""
		SELECT {} FROM {} WHERE {} >= {}
		""".format(primary,table,filter,datefilter)

		#print(q)
		# Get data and put it in a list
		if q:
			r=[]
			res=self.query(q)
			#r = [i[0] for i in res]
			for row in res:
				r+=(row[0],)
				if row[0] not in self.inserted_id:
					diff.append(row[0])
			#s = set(r)
			#diff = [x for x in self.inserted_id if x not in s]

		# Count deleted
		nbdel=len(diff)

		# Finally delete
		for delid in diff:
			q="DELETE FROM {} WHERE {}=\"{}\" LIMIT 1".format(table,primary,delid)
			#print(q)
			self.query(q,commit=True)

		# Log what have been done
		if nbdel>0:
			self.Logs.Log("WARNING","{} records deleted, as you requested with 'delete_after' setting.".format(nbdel))
		else:
			self.Logs.Log("DEBUG","No record deleted, but setting 'delete_after' is enabled.")

		return diff
		#print("INSIDE")
		#print(r)
		#print("INSERTED")
		#print(self.inserted_id)
		#print("DIFFERENCE")
		#print(diff)
		return r
#
	def iter_row(self,cursor, size=10):
		while True:
			rows = cursor.fetchmany(size)
			if not rows:
				break
			for row in rows:
				yield row
#
	def close(self):
		if self.conn:
			self.conn.close()
#
	def getStartDate(self):
		startdate=False
		# Take start date given in global config
		if 'startdate' in self.CONFIG:
			startdate=self.CONFIG['startdate']
			self.loglevel='WARNING'
                # Or pick up the start date provided in the query
		else:
			startdate=self.GLOBALCONFIG['queries']['startdate']
			self.loglevel='DEBUG'
		#if filterdate:
		#	self.Logs.Log(loglevel,"Query will be filtered by results starting from {startdate}".format(startdate=filterdate))
		return startdate

	def setIntervalMonth(self,intervalmonth):
		self.intervalmonth=intervalmonth

	def getIntervalMonth(self):
		intervalmonth=False
		# Pick up intervalmonth provided by cli if exists
		if self.intervalmonth:
			intervalmonth=self.intervalmonth
			self.loglevel='DEBUG'
		# Interval provided in global config if it exists
		elif 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			self.loglevel='WARNING'
		# Or pick up interval in query config, if it exists
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			self.loglevel='DEBUG'
		#if intervalmonth:
		#	self.Logs.Log(loglevel,"Query will be filtered to dataset fresher than {intervalmonth} month".format(intervalmonth=intervalmonth))
		return intervalmonth
#
	def addStartDate(self):
		q=self.CONFIG['query']
		#print(q)
		startdatepattern=self.GLOBALCONFIG['advanced']['startdatepattern']
		intervalmonthpattern=self.GLOBALCONFIG['advanced']['intervalmonthpattern']

		"""
		# Take start date given in global config
		if 'startdate' in self.CONFIG:
			filterdate=self.CONFIG['startdate']
			loglevel='WARNING'
		# Or pick up the start date provided in the query
		else:
			filterdate=self.GLOBALCONFIG['queries']['startdate']
			loglevel='DEBUG'
		# Interval provided in global config if it exists
		if 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			loglevel='WARNING'
		# Or pick up interval in query config, if it exists
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			loglevel='DEBUG'
		"""

		# If none of filter is present, display a warning
		if intervalmonthpattern not in q and startdatepattern not in q:
			self.Logs.Log("WARNING","Query does not contain a date filter. It could really impact performance.")
		# Case of absolute start date
		if startdatepattern in q:
			startdate=self.getStartDate()
			self.Logs.Log(self.loglevel,"Query will be filtered by results starting from {startdate}".format(startdate=startdate))
			# Replace pattern by a start date in configuration
			q=q.replace(startdatepattern, str(filterdate))
		if intervalmonthpattern in q:
			intervalmonth=self.getIntervalMonth()
			self.Logs.Log(self.loglevel,"Query will be filtered to dataset fresher than {intervalmonth} month".format(intervalmonth=intervalmonth))
			# Replace pattern by an interval provided in configuration
			q=q.replace(intervalmonthpattern, str(intervalmonth))
		#print(q)
		return q
#
	def has_duplicates(self,values):
		if len(values) != len(set(values)):
			return len(values) - len(set(values))
		else:
			return False
#
	def list_duplicates(self,values):
		duplicates = [val for val in values if values.count(val) > 1]
		unique_duplicates = list(set(duplicates))
		return unique_duplicates

	def LogRun(self,id):
		table="logs"
		# Eventually create table and commit
		self.query("CREATE TABLE IF NOT EXISTS {table} (id VARCHAR(255) PRIMARY KEY COLLATE utf8_bin, datetime datetime NOT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin".format(table=table),True)
		# Update timestamp
		self.query("INSERT INTO {table}(id,datetime) VALUES('{id}',NOW()) ON DUPLICATE KEY UPDATE datetime=NOW()".format(table=table,id=id),True)
