#!/usr/bin/env python
#-*- coding: utf-8 -*-

# Source : https://stackoverflow.com/questions/39149243/how-do-i-connect-to-a-sql-server-database-with-python
import pymssql

class MssqlDB():

	def __init__(self,GLOBALCONFIG,CONFIG,Logs):
		# Get Global Settings (config/config.yaml)
		self.GLOBALCONFIG=GLOBALCONFIG
		# Store individual Settings
		self.CONFIG=CONFIG
		# Store an object for logs
		self.Logs = Logs
		try:
			self.conn = pymssql.connect(server=self.CONFIG['db']['host'], user=self.CONFIG['db']['user'], password=self.CONFIG['db']['password'], database=self.CONFIG['db']['database'])
		except Exception as e:
			self.Logs.Log("ERROR","Unable to connect to DB with options {}".format(self.CONFIG['db']))
			self.Logs.Log("DEBUG","Error was {}".format(e))
			self.conn=False
#
	def getQueryModel(self,q,table):
		"""
		"CREATE TABLE `test` 
			`Numero` int(11) NOT NULL,
			`Nom` varchar(250) COLLATE utf8_bin NOT NULL,
			`Prenom` varchar(250) COLLATE utf8_bin NOT NULL,
			`Datedenaissance` date NOT NULL\n) 
			ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin
		"""
		q="CREATE TABLE {table}\n".format(table=table)
		count=0
		q+="(\n"
		for key,datatype in self.CONFIG['fields'].items():
			if count>0:
				q+=",\n"
			q+="`{key}` {datatype}".format(key=key,datatype=datatype)
			count+=1
		q+=")\n"
		q+="ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"
		return q
#
	def columns(self,table):
		pass
	"""
	Return an ordered dict of column name and column type
	"""
	def getfields(self,fields):
		return fields
#
#
	def query(self,q, commit=False,multi=False):
		table=()
		cursor = self.conn.cursor()
		#print(q)
		try:
			# Run query
			cursor.execute(q)
			# Get affected rows
			allrows = cursor.fetchall()
			self.rowcount=cursor.rowcount
			for row in allrows:
				table+=(row,)
				#print(row)
				if commit:
					try:
						self.conn.commit()
					except:
						self.Logs.Log("[ERROR]", "Unable to commit changes, rolling back.")
						self.conn.rollback()
		except Error as err:
				if err.errno == 1068:
					self.Logs.Log("WARNING","MSSql said : {e}".format(e=err.msg))
				else:
					self.Logs.Log("ERROR","MSSql said : Error {n} - {e}".format(n=err.errno,e=err.msg))
		# Get affected rows and last affected row ID
		#self.rowcount=cursor.rowcount
		# Give results
		return table

#
	def insert(self,d,table):
		counta=0
		self.insertrowcount=0
		self.rowcount=0
		for i in d:
			q="REPLACE INTO {table} VALUES(".format(table=table)
			#print(i)
			countb=0
			for f in i:
				# Try to decode bytearray field (imported from mysql), or convert it to string
				try:
					f = f.decode()
				except:
					f=str(f)
				# Escaping string
				f=self.conn.converter.escape(f)
				# Add comma to field except if it's the first one
				if countb>0:
					q+=","
				# Add ' to protect field
				q+="'"+str(f)+"'"
				# +1
				countb+=1
			# Close query
			q+=")"
			#print(q)
			counta+=1
			self.query(q,commit=True)
			self.insertrowcount+=self.rowcount
			#if counta==1:
			#	break
#
	def close(self):
		self.conn.close()
#
	"""
	def addStartDate(self):
		q=self.CONFIG['query']
		startdatepattern=self.GLOBALCONFIG['advanced']['startdatepattern']
		#print(q)
		# If query provide a custom startdate, use it. Otherwise, fallback to global startdate
		if 'startdate' in self.CONFIG: filterdate=self.CONFIG['startdate']
		else: filterdate=self.GLOBALCONFIG['queries']['startdate']
		# If pattern found in query
		if startdatepattern in q:
			# Formatting startdate for MSSQL
			filterdate=str(filterdate).split(' ')[0]
			filterdate=filterdate.replace('-','')
			self.Logs.Log("DEBUG","Query will be filtered by results starting from {startdate}".format(startdate=filterdate))
			#self.Logs.Log("DEBUG","Replacing pattern to start date in query.")
			# Replace pattern by a start date in configuration
			q=q.replace(startdatepattern, str(filterdate))
		else:
			self.Logs.Log("WARNING","Query does not contain a date filter. It could really impact performance.")
		#print(q)
		return q
	"""
#
	def getStartDate(self):
		startdate=False
		# Take start date given in global config
		if 'startdate' in self.CONFIG:
			startdate=self.CONFIG['startdate']
			self.loglevel='WARNING'
			# Or pick up the start date provided in the query
		else:
			startdate=self.GLOBALCONFIG['queries']['startdate']
			self.loglevel='DEBUG'
		return startdate
#
	def setIntervalMonth(self,intervalmonth):
		self.intervalmonth=intervalmonth

	def getIntervalMonth(self):
		intervalmonth=False
		# Pick up intervalmonth provided by cli if exists
		if self.intervalmonth:
			intervalmonth=self.intervalmonth
			self.loglevel='DEBUG'
		# Interval provided in global config if it exists
		elif 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			self.loglevel='WARNING'
		# Or pick up interval in query config, if it exists
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			self.loglevel='DEBUG'
		return intervalmonth
#
	def addStartDate(self):
                q=self.CONFIG['query']
                startdatepattern=self.GLOBALCONFIG['advanced']['startdatepattern']
                #print(startdatepattern)
                intervalmonthpattern=self.GLOBALCONFIG['advanced']['intervalmonthpattern']
                #print(intervalmonthpattern)

                # Take start date given in global config
                if 'startdate' in self.CONFIG:
                        filterdate=self.CONFIG['startdate']
                        loglevel='WARNING'
                # Or pick up the start date provided in the query
                else:
                        filterdate=self.GLOBALCONFIG['queries']['startdate']
                        loglevel='DEBUG'
                # Interval provided in global config if it exists
                if 'intervalmonth' in self.CONFIG:
                        intervalmonth=self.CONFIG['intervalmonth']
                        loglevel='WARNING'
                # Or pick up interval in query config, if it exists
                else:
                        intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
                        loglevel='DEBUG'

                # If none of filter is present, display a warning
                if intervalmonthpattern not in q and startdatepattern not in q:
                        self.Logs.Log("WARNING","Query does not contain a date filter. It could really impact performance.")
                # Case of absolute start date
                if startdatepattern in q:
                        self.Logs.Log(loglevel,"Query will be filtered by results starting from {startdate}".format(startdate=filterdate))
                        # Replace pattern by a start date in configuration
                        q=q.replace(startdatepattern, str(filterdate))
                if intervalmonthpattern in q:
                        self.Logs.Log(loglevel,"Query will be filtered to results fresher than {intervalmonth} month".format(intervalmonth=intervalmonth))
                        # Replace pattern by an interval provided in configuration
                        q=q.replace(intervalmonthpattern, str(intervalmonth))
                #print(q)
                return q
