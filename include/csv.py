#!/usr/bin/env python
#-*- coding: utf-8 -*-
#### Web sources
import csv
import os
from datetime import datetime
#
class csvDB():
	"""
	Create a connection to DB
	"""
	def __init__(self,GLOBALCONFIG,CONFIG,Logs):
		# Get Global Settings (config/config.yaml)
		self.GLOBALCONFIG=GLOBALCONFIG
		# Store individual Settings
		self.CONFIG=CONFIG
		# Sore an object for logs
		self.Logs = Logs
		# Ghost value - unused in this object
		self.conn=True
#
	"""
	Create a table based on schema given in query settings. Used to host data collected from query.
	"""
	def getQueryModel(self,q,table):
		# Init field list containing dates
		self.datelist=()
		self.datetimelist=()
		# Specific of CSV format, we construct the fields on the fly
		self.fields={}
		# Get first line
		firstline=self.query(False,False,False,True)
		if not firstline: return False
		#print("First Line {}".format(firstline))
		q="CREATE TABLE {table}\n".format(table=table)
		count=0
		q+="(\n"
		# If header is present in file, base future fields on these fields
		if 'header' in self.CONFIG and self.CONFIG['header']:
			# Get fields from first line
			dbfields=firstline
		# If no header in file, base future fields on config file
		elif 'header' not in self.CONFIG or not self.CONFIG['header']:
			# Get fields from query config
			dbfields=self.CONFIG['fields']
			# Create a list of fields indexes (usefull to navigate with dict IDs)
			fieldslist=list(dbfields)
		# Loop thru future DB fields
		for dbfield in dbfields:
			# Ignore an empty value on header
			#if dbfield=="" and 'header' in self.CONFIG and self.CONFIG['header']: continue
			#elif dbfield=="" and ('header' not in self.CONFIG or not self.CONFIG['header']): dbfield="datafield"+str(count)
			# Replace an empty value by an autogenerated name
			if dbfield=="":dbfield="datafield"+str(count)
			# Get value from element
			if 'header' in self.CONFIG and self.CONFIG['header']:
				dbfield=dbfield
			# Or get value from config diect (based on dict converted to list, to extract ID)
			elif 'header' not in self.CONFIG or not self.CONFIG['header']:
				dbfield=fieldslist[count]
			# Construct query
			if count>0:
				q+=",\n"
			# Datatype order :  1. CONFIG for each field independantly 2. Value for all fields 3. default
			if 'fields' in self.CONFIG and dbfield in self.CONFIG['fields']:
				datatype=self.CONFIG['fields'][dbfield]
			# Global datatype for all other fields
			elif 'datatype' in self.CONFIG:
				datatype=self.CONFIG['datatype']
			# Or default one
			else:
				datatype="varchar(250) COLLATE utf8_bin"
			# Save index of column with date inside
			if datatype.find('date') != -1:
				self.datelist+=(count,)
			if datatype.find('datetime') != -1:
				self.datetimelist+=(count,)
			#
			# Set field and type in SQL
			q+="`{dbfield}` {datatype}".format(dbfield=dbfield,datatype=datatype)
			# Set a dict of field and type (create on the fly the "fields" list)
			self.fields[dbfield]=datatype
			count+=1
		q+=")\n"
		q+="ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"
		#print(self.fields)
		if 'dateformat' in self.CONFIG and len(self.datelist)>0:
			self.Logs.Log("WARNING","Fields {fields} has been detected as DATE and will be converted from {dateformat} to mysql default format".format(fields=self.datelist,dateformat=self.CONFIG['dateformat']))
		if 'dateformat' in self.CONFIG and len(self.datetimelist)>0:
			self.Logs.Log("WARNING","Fields {fields} has been detected as DATETIME and will be converted from {dateformat} to mysql default format".format(fields=self.datetimelist,dateformat=self.CONFIG['dateformat']))
		#print("Query model : "+q)
		return q
#
	"""
	how to describe this
	"""
	def columns(self,table):
		# Return self generated fields (on the fly by the GetQueryModel method)
		return self.fields
#
	"""
	Return an ordered dict of column name and column type
	"""
	def getfields(self,fields):
		return self.fields
#
	"""
	Send query to database server
	"""
	def query(self,q=False, commit=False,multi=False,getheader=False):
		table=()
		self.rowcount=0
		# Read every file in config
		for file in self.CONFIG['files']:
			path='{}/{}'.format(self.GLOBALCONFIG['advanced']['depot'],file)
			if not os.path.isfile(path):
				self.Logs.Log("WARNING","Updable to open file {}. Ignoring".format(path))
				return False
			# Open every file specified file
			with open(path, 'r') as csvfile:
				self.Logs.Log("DEBUG","Opening file {}".format(path))
				# Count all lines
				lines=0
				# Count lines without header
				datalines=0
				# Open with or w/o string separator
				if 'stringseparator' in self.CONFIG and len(self.CONFIG['stringseparator'])>0: reader = csv.reader(csvfile,delimiter=self.CONFIG['separator'],quotechar=self.CONFIG['stringseparator'])
				else: reader = csv.reader(csvfile,delimiter=self.CONFIG['separator'])
				# Each line
				for row in reader:
					#print("Ligne {} : {}".format(lines,row))
					cfields=0
					newfields=()
					for field in row:
						#print("Working on field "+field+" with id "+str(cfields))
						# Convert date if requested
						if 'dateformat' in self.CONFIG and (lines>0 or not self.CONFIG['header']) and len(field)>0:
							#print(" cfields {} and self.datelist {}".format(cfields,self.datelist))
							if cfields in self.datelist:
								#print("ici")
								#self.Logs.Log("WARNING","Converting date {} into mysql format".format(field))
								d = datetime.strptime(field, self.CONFIG['dateformat'])
								field=d.strftime('%Y-%m-%d')
								#self.Logs.Log("WARNING","Converted in {}".format(field))
							if cfields in self.datetimelist:
								#self.Logs.Log("DEBUG","Converting datetime {} into mysql format".format(field))
								d = datetime.strptime(field, "'"+self.CONFIG['dateformat']+"'")
								field=d.strftime('%Y-%m-%d %H:%M:%s')
								#self.Logs.Log("WARNING","Converted in {}".format(field))
						# Add field to the new list
						newfields+=(field,)
						cfields+=1
					# Add new row to the final table (except if the first line is the header)
					#print(getheader)
					if lines>0 or (getheader and 'header' in self.CONFIG and self.CONFIG['header']) or ('header' in self.CONFIG and not self.CONFIG['header']):
						table+=(newfields,)
						datalines+=1
					else:
						self.Logs.Log("WARNING","Ignoring first line which is defined to be the header {}".format(getheader))
					# Increment lines
					lines+=1
				# To get header just pick up the first line
				if getheader: return table[0]
			# Count total rowcount
			self.rowcount += datalines
		# Debug
		#print(table)
		return table
#
	"""
	?
	"""
	def insert(self,d,table):
		pass
#
	def close(self):
		pass
#
	"""
	Used to replace pattern by global start date. Avoid a big amount of data
	"""
	def addStartDate(self):
		pass

	def getStartDate(self):
		startdate=False
		# Take start date given in global config
		if 'startdate' in self.CONFIG:
			startdate=self.CONFIG['startdate']
			self.loglevel='WARNING'
			# Or pick up the start date provided in the query
		else:
			startdate=self.GLOBALCONFIG['queries']['startdate']
			self.loglevel='DEBUG'
		return startdate
#
	def setIntervalMonth(self,intervalmonth):
		self.intervalmonth=intervalmonth
#
	def getIntervalMonth(self):
		intervalmonth=False
                # Pick up intervalmonth provided by cli if exists
		if self.intervalmonth:
			intervalmonth=self.intervalmonth
			self.loglevel='DEBUG'
		# Interval provided in global config if it exists
		elif 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			self.loglevel='WARNING'
		# Or pick up interval in query config, if it exists
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			self.loglevel='DEBUG'
		return intervalmonth
