#!/usr/bin/env python
#-*- coding: utf-8 -*-
#### Web sources
# Source : https://www.geeksforgeeks.org/oracle-database-connection-in-python/
# Source : http://chicoree.fr/w/Acc%C3%A9der_%C3%A0_une_base_de_donn%C3%A9es_Oracle_%C3%A0_partir_de_Python
#### To load Oracle Library :
# First run : export ORACLE_HOME="/home/FHM/{user}/consolidate/lib/oracle_instantclient_12_2"
# Then run : export LD_LIBRARY_PATH="${ORACLE_HOME}:${LD_LIBRARY_PATH}"
#
#
try:
	import cx_Oracle
except Exception as e:
	print("Please install cx_Oracle with : \"pip3 install cx_Oracle\" or similar.")
	print("Please also install oracle SDK")

class OracleDB():
	"""
	Create a connection to DB
	"""
	def __init__(self,GLOBALCONFIG,CONFIG,Logs):
		# Get Global Settings (config/config.yaml)
		self.GLOBALCONFIG=GLOBALCONFIG
		# Store individual Settings
		self.CONFIG=CONFIG
		# Sore an object for logs
		self.Logs = Logs
		try:
			dsn=cx_Oracle.makedsn(self.CONFIG['db']['host'], self.CONFIG['db']['port'], service_name=self.CONFIG['db']['instance'])
			self.conn = cx_Oracle.connect(user=self.CONFIG['db']['user'],password=self.CONFIG['db']['password'],dsn=dsn)
		except Exception as e:
			self.Logs.Log("ERROR","Unable to connect to DB with options {}".format(self.CONFIG['db']))
			self.Logs.Log("DEBUG","Error was {}".format(e))
			self.conn=False
#
	"""
	Create a table based on schema given in query settings. Used to host data collected from query.
	"""
	def getQueryModel(self,q,table):
		q="CREATE TABLE {table}\n".format(table=table)
		count=0
		q+="(\n"
		for key,datatype in self.CONFIG['fields'].items():
			if count>0:
				q+=",\n"
			q+="`{key}` {datatype}".format(key=key,datatype=datatype)
			count+=1
		q+=")\n"
		q+="ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin"
		return q
#
	def columns(self,table):
		pass
#
	"""
	Return an ordered dict of column name and column type
	"""
	def getfields(self,fields):
		return fields
#
	"""
	Send query to database server
	"""
	def query(self,q, commit=False,multi=False):
		table=()
		#cursor = self.conn.cursor()
		cursor = self.conn.cursor()
		#print(q)
		try:
			cursor.execute(q)
			#row = cursor.fetchall()
			for row in cursor:
				table+=(row,)
				if commit:
					try:
						self.conn.commit()
					except:
						self.Logs.Log("[ERROR]", "Unable to commit changes, rolling back.")
						self.conn.rollback()
		except cx_Oracle.DatabaseError as err:
			errno=str(err).split(':')
			errno=errno[0]
			if errno == 'ORA-00933':
				self.Logs.Log("ERROR","Oracle said : {e}".format(e=err))
				self.Logs.Log("ERROR","You could try to remove the ending semicolon at the end of the query")
			else:
				self.Logs.Log("ERROR","Oracle said : {e}".format(e=err))
		# Get affected rows and last affected row ID
		self.rowcount=cursor.rowcount
		# Return data
		return table

#
	"""
	?
	"""
	def insert(self,d,table):
		pass
#
	def close(self):
		try:
			if self.conn:
				self.conn.close()
			if self.cursor:
				self.cursor.close()
		except:
			pass
#
	def getStartDate(self):
		startdate=False
		# Take start date given in global config
		if 'startdate' in self.CONFIG:
			startdate=self.CONFIG['startdate']
			self.loglevel='WARNING'
			# Or pick up the start date provided in the query
		else:
			startdate=self.GLOBALCONFIG['queries']['startdate']
			self.loglevel='DEBUG'
		return startdate
#
	def setIntervalMonth(self,intervalmonth):
		self.intervalmonth=intervalmonth
#
	def getIntervalMonth(self):
		intervalmonth=False
		# Pick up intervalmonth provided by cli if exists
		if self.intervalmonth:
			intervalmonth=self.intervalmonth
			self.loglevel='DEBUG'
		# Interval provided in global config if it exists
		elif 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			self.loglevel='WARNING'
		# Or pick up interval in query config, if it exists
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			self.loglevel='DEBUG'
		return intervalmonth
#
	"""
	Used to replace pattern by global start date. Avoid a big amount of data
	"""
	def addStartDate(self):
		q=self.CONFIG['query']
		startdatepattern=self.GLOBALCONFIG['advanced']['startdatepattern']
		#print(startdatepattern)
		intervalmonthpattern=self.GLOBALCONFIG['advanced']['intervalmonthpattern']
		#print(intervalmonthpattern)

		# If query provide a custom config, use it. Otherwise, fallback to global config
		if 'startdate' in self.CONFIG:
			filterdate=self.CONFIG['startdate']
			loglevel='WARNING'
		else:
			filterdate=self.GLOBALCONFIG['queries']['startdate']
			loglevel='DEBUG'
		if 'intervalmonth' in self.CONFIG:
			intervalmonth=self.CONFIG['intervalmonth']
			loglevel='WARNING'
		else:
			intervalmonth=self.GLOBALCONFIG['queries']['intervalmonth']
			loglevel='DEBUG'

		# If none of filter is present, display a warning
		if intervalmonthpattern not in q and startdatepattern not in q:
			self.Logs.Log("WARNING","Query does not contain a date filter. It could really impact performance.")
		# Case of absolute start date
		if startdatepattern in q:
			self.Logs.Log(loglevel,"Query will be filtered by results starting from {startdate}".format(startdate=filterdate))
			# Replace pattern by a start date in configuration
			q=q.replace(startdatepattern, str(filterdate))
		if intervalmonthpattern in q:
			self.Logs.Log(loglevel,"Query will be filtered to results fresher than {intervalmonth} month".format(intervalmonth=intervalmonth))
			# Replace pattern by an interval provided in configuration
			q=q.replace(intervalmonthpattern, str(intervalmonth))
		#print(q)
		return q
